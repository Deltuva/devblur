$(function() {
  /* ==========================================================================
     Google Map Initialize
  ========================================================================== */
    function map() {
        var cord = {
                lat: 54.728420,
                lng: 25.235371
            },

            mapOptions = {
                center: cord,
                zoom: 17,
                scrollwheel: false,
                mapTypeControl: false
            };

        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        var image = new google.maps.MarkerImage('../../img/marker.png', null, null, null, new google.maps.Size(60, 60));
        new google.maps.Marker({
            position: cord,
            map: map,
            title: 'itBlur',
            icon: image,
            optimized: false
        });
    }
    google.maps.event.addDomListener(window, 'load', map);
    /* ==========================================================================
       Toggle meniu
    ========================================================================== */
    $(".toggle-mnu").click(function() {
        $(this).toggleClass("on");
        $(".main-mnu").slideToggle();
        return false;
    });

    /* ==========================================================================
       Header
    ========================================================================== */
    //$(".slider-wrap").slideDown();
    var owlHead = $(".slider");
    owlHead.owlCarousel({
        loop: true,
        items: 1,
        itemClass: "slide-wrap",
        nav: true,
        navText: "",
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: false
    });

    owlHead.trigger('refresh.owl.carousel');

    $(".next").click(function() {
        owlHead.trigger('next.owl.carousel');
    })
    $(".prev").click(function() {
        owlHead.trigger('prev.owl.carousel');
    });

    /* ==========================================================================
       Features
    ========================================================================== */
    $(".slider-features").owlCarousel({
        loop: true,
        items: 1,
        itemClass: "slide-wrap",
        nav: true,
        navText: ""
    });

    $(".menu-toggle-text-up")
        .hide();

    $(".mobile-feature-mnu").click(function() {
        if ($(".mobile-feature-mnu")
            .hasClass('slide-up')) {
            $(".mobile-feature-mnu")
                .addClass('slide-down');
            $(".mobile-feature-mnu")
                .removeClass('slide-up');
            $(this)
                .find('i')
                .removeClass('fa-sort-up')
                .addClass('fa-sort-down');
            $(".feature-mnu").slideUp('fast');
            $(".menu-toggle-text-down").show();
            $(".menu-toggle-text-up").hide();
        } else {
            $(".mobile-feature-mnu")
                .removeClass('slide-down');
            $(this)
                .find('i')
                .removeClass('fa-sort-down')
                .addClass('fa-sort-up');
            $(".mobile-feature-mnu")
                .addClass('slide-up');
            $(".feature-mnu").slideDown('fast');
            $(".menu-toggle-text-up").show();
            $(".menu-toggle-text-down").hide();
        }
    });

    /* ==========================================================================
       Brands
    ========================================================================== */
    $(".slider-brand").owlCarousel({
        loop: true,
        itemClass: "slide-wrap",
        margin: 30,
        nav: false,
        navText: "",
        dots: false,
        responsive: {
            0: {
                items: 1,
            },
            520: {
                items: 1,
            },
            560: {
                items: 2,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
            }
        },
        smartSpeed: 2000,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false
    });

    /* ==========================================================================
       Move to Top button
    ========================================================================== */
    $("body").on("click", ".scroll-up", function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
    });
    $("body").append('<div class="scroll-up"><i class="fa fa-angle-up"></i></div>');

    $(window).scroll(function() {
        if ($(this).scrollTop() > $(this).height()) {
            $(".scroll-up").addClass("active");
        } else {
            $(".scroll-up").removeClass("active");
        }
    });

});
